# Atelier intégration de données - Hackathon Aussois 2022

L’objectif de l’atelier est d’intégrer et d’analyser des données omiques hétérogènes via des approches statistiques en s’appuyant sur les packages R MOFA2 (https://biofam.github.io/MOFA2/)  et mixOmics (http://mixomics.org/). Pour un cas d’étude, nous proposons d’analyser conjointement des données métabolomiques et transcriptomiques obtenues à différents points du développement de 2 variétés de colzas soumises un stress hydrique et parasitique (Bianchetti et al. Data in Brief 2021 10.1016/j.dib.2021.107392 ;  et 10.1016/j.dib.2021.107247).

## Datasets

* Données Vanessa (2 datasets: transcriptomique + métabolomique) 
* Données Kevin (4 datasets: 2 RNAseq + 2 metabarcoding (16S et 18S))

## Méthodologies

* MixOmics (v6.22 [Bioconductor - mixOmics](https://bioconductor.org/packages/release/bioc/html/mixOmics.html))
* iClusterPlus (v1.34 B[ioconductor - iClusterPlus](https://bioconductor.org/packages/release/bioc/html/iClusterPlus.html))
* MOFA2 (v1.8 [Bioconductor - MOFA2](https://www.bioconductor.org/packages/release/bioc/html/MOFA2.html))

## Analyses

### mixOmics

```bash
# script de pré-traitement
Donnees_Vanessa_pretraitement.R

# datasets Vanessa
filteredData2.RData

# scripts d'analyses mixOmics
Donnees_Vanessa_mixomics.Rmd

```

### Restitution de l'atelier

```bash
restitution_atelier2_08.12.22.pdf

```

### session info

```{r}
R version 4.2.2 Patched (2022-11-10 r83330)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 20.04.5 LTS

MOFA2_1.8.0         
mixOmics_6.22.0 
iClusterPlus_1.34.1 
corrplot_0.92 
DESeq2_1.38.1

```